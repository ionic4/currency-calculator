import { Component, OnInit, } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  pounds: number;
  euros: number;
  ngOnInit(): void {
    console.log("ngOnInit");
    //this.pounds = 0;
    //this.euros = 0;
  }

  calculate() {
    console.log("calculate " + this.pounds);
    this.euros = this.pounds * 1.10;
  }
}
